#include <stdio.h>
#include <stdlib.h>

void printList(int* in, int length)
{
	if (length == 0) {
		printf("[]\n");
		return;
	}

	int i;
	printf("[%i", in[0]);
	for (i = 1; i < length; ++i) {
		printf(", %i", in[i]);
	}
	printf("]\n");
}

int compareInts(int left, int right)
{
	return left < right;
}

int partition(int* array, int left, int right, int pivotIndex)
{
	int pivotValue = array[pivotIndex];

	// Move the pivot to the end
	int tmp = array[right];
	array[right] = array[pivotIndex];
	array[pivotIndex] = tmp;

	int storeIndex = left;

	// Put items < than the pivot before the pivot index
	int i;
	for (i = left; i < right; ++i) {
		if (array[i] < pivotValue) {
			tmp = array[i];
			array[i] = array[storeIndex];
			array[storeIndex] = tmp;
			++storeIndex;
		}
	}

	// Then put the pivot back (swap first number larger than pivot)
	tmp = array[right];
	array[right] = array[storeIndex];
	array[storeIndex] = tmp;
	return storeIndex;
}

void quicksort(int* array, int left, int right)
{
	if (left < right) {
		// Choose a new pivot somewhere between right and left
		int pivotIndex = left + (rand() % (right - left));

		// Partition the array
		int newPivotIndex = partition(array, left, right, pivotIndex);

		// Recursively sort
		quicksort(array, left, newPivotIndex - 1);
		quicksort(array, newPivotIndex + 1, right);
	}
}

int main(void)
{
	int dataSize = 100;
	int* data = (int*) malloc(dataSize * sizeof(int));

	int i;
	for (i = 0; i < dataSize; ++i) {
		data[i] = rand() % 10;
	}

	printList(data, dataSize);
	quicksort(data, 0, dataSize - 1);
	printList(data, dataSize);

	free(data);
	return 0;
}