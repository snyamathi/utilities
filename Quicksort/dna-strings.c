/*
This is a special quicksort meant for DNA strings that WRAP AROUND the end
of the genome.  The reason for this is so that we can employ the Burrows-
Wheeler Transform for DNA Sequence Alignment.  The property here is that
strings there is an invisible $ at the end of the genome which is lexico-
graphically smaller than any base.
*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <memory.h>

char* genome = "GGGGTCTCGGGGTTTTACGACTAGCATCAGCATCATCTATCTACTATCGAGCGACTACGATCAGCTACGACTACGATCGACTAGCATCAGCTACGATCGT";
int genomeSize = 100;

void printList(int* genomePointers, int length)
{
	if (length == 0) {
		printf("[]\n");
		return;
	}

	char* tmp = (char*) malloc((genomeSize+1) * sizeof(char));
	tmp[genomeSize] = '\0';

	printf("[");
	int i;
	for (i = 0; i < length; ++i) {
		memcpy(tmp, genome + genomePointers[i], genomeSize - genomePointers[i]);
		memcpy(tmp + genomeSize - genomePointers[i], genome, genomePointers[i]);
		printf("%s, ", tmp);
	}
	printf("]\n");
	free(tmp);
}

int leftLessThanRight(int left, int right)
{
	if (left == right) {
		return false;
	}

	assert (!(left == genomeSize || right == genomeSize));

	int i;
	for (i = 0; i < genomeSize; ++i) {
		// End of genome is lexicographically lowest char.
		if (left == genomeSize) {
			return true;
		} else if (right == genomeSize) {
			return false;
		} else if (genome[left] != genome[right]) {
			return genome[left] < genome[right];
		}

		left += 1;
		right += 1;
	}

	// You shouln't get to here, a tie is NOT possible
	fputs("ERR: Sequence compare failed", stderr);
	exit(1);
	return false;
}

int partition(int* array, int left, int right, int pivotIndex)
{
	int pivotValue = array[pivotIndex];

	// Move the pivot to the end
	int tmp = array[right];
	array[right] = array[pivotIndex];
	array[pivotIndex] = tmp;

	int storeIndex = left;

	// Put items < than the pivot before the pivot index
	int i;
	for (i = left; i < right; ++i) {
		if (leftLessThanRight(array[i], pivotValue)) {
			tmp = array[i];
			array[i] = array[storeIndex];
			array[storeIndex] = tmp;
			++storeIndex;
		}
	}

	// Then put the pivot back (swap first number larger than pivot)
	tmp = array[right];
	array[right] = array[storeIndex];
	array[storeIndex] = tmp;
	return storeIndex;
}

void quicksort(int* array, int left, int right)
{
	if (left < right) {
		// Choose a new pivot somewhere between right and left
		int pivotIndex = left + (rand() % (right - left));

		// Partition the array
		int newPivotIndex = partition(array, left, right, pivotIndex);

		// Recursively sort
		quicksort(array, left, newPivotIndex - 1);
		quicksort(array, newPivotIndex + 1, right);
	}
}

int main(void)
{
	int genomePointers[100];
	int i;
	for (i = 0; i < 100; ++i) {
		genomePointers[i] = i;
	}
	int length = 100;

	printList(genomePointers, length);
	quicksort(genomePointers, 0, length -1);
	printList(genomePointers, length);

	return 0;
}